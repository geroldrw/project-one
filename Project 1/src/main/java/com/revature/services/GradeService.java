package com.revature.services;

import java.util.Set;

import com.revature.beans.Grade;

public interface GradeService {
	public int addGrade(Grade g);
	public Set<Grade> getAllGrades();
	public Grade getGradeById(int id);
	public Set<Grade> getPendingGrades();
	public Set<Grade> getAcceptedGrades();
	public void updateGrade(Grade g);
	public void deleteGrade(Grade g);
}
