package com.revature.services;

import java.util.Set;

import com.revature.beans.Info;

public interface InfoService {
	// create
	public int addInfo(Info p);
	// select
	public Info getInfoById(int id);
	public Set<Info> getAllinfo();
	// update
	public void updateInfo(Info p);
	// delete
	public void deleteInfo(Info p);

}

