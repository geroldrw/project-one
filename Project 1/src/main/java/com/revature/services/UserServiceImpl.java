package com.revature.services;

import java.util.Set;

import com.revature.beans.User;
import com.revature.data.UserDAO;
import com.revature.data.UserOracle;

public class UserServiceImpl implements UserService {
private static UserDAO userDao;
	
	public UserServiceImpl() {
		userDao = new UserOracle();
	}

	//create
	@Override
	public Integer addUser(User p) {
		return userDao.add(p);
	}

	//select
	@Override
	public User getUSerById(Integer id) {
		return userDao.getById(id);
	}

	@Override
	public User getUserByUsernameAndPassword(String username, String password) {
		return userDao.getUserByUsernameAndPassword(username, password);
	}

	@Override
	public Set<User> getAllUsers() {
		return userDao.getAll();
	}

	//update
	@Override
	public void updateUser(User p) {
		userDao.update(p);
		
	}

	//delete
	@Override
	public void deleteUser(User p) {
		userDao.delete(p);
		
	}
}
