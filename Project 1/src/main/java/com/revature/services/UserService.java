package com.revature.services;

import java.util.Set;

import com.revature.beans.User;

public interface UserService {
	// create
				public Integer addUser(User p);
				// read
				public User getUSerById(Integer id);
				public User getUserByUsernameAndPassword(String username, String password);
				public Set<User> getAllUsers();
				// update
				public void updateUser(User p);
				// delete
				public void deleteUser(User p);
}
