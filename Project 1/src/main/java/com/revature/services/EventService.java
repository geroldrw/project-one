package com.revature.services;

import java.util.Set;

import com.revature.beans.Event;

public interface EventService {
	
	public int add(Event e);
	public Set<Event> getAll();
	public Event getById(int id);
	public void update(Event e);
	public void delete(Event e);
}
