package com.revature.services;

import java.util.Set;

import com.revature.beans.Info;
import com.revature.data.InfoDAO;
import com.revature.data.InfoOracle;



public class InfoServiceImpl implements InfoService {
	private static InfoDAO infoDao;
	
	public InfoServiceImpl() {
		infoDao = new InfoOracle();
	}

	//create
	@Override
	public int addInfo(Info p) {
		return infoDao.add(p);
	}

	//select
	@Override
	public Info getInfoById(int id) {
		return infoDao.getById(id);
	}

	@Override
	public Set<Info> getAllinfo() {
		return infoDao.getAll();
	}

	//update
	@Override
	public void updateInfo(Info p) {
		infoDao.update(p);
		
	}

	//delete
	@Override
	public void deleteInfo(Info p) {
		infoDao.delete(p);
		
	}



}
