package com.revature.services;

import java.util.Set;

import com.revature.beans.Event;
import com.revature.data.EventDAO;
import com.revature.data.EventOracle;

public class EventServiceImpl implements EventService {
	
	private static EventDAO eventDao;
	
	public EventServiceImpl()
	{
		eventDao = new EventOracle();
	}

	public int add(Event e) {
		return eventDao.add(e);
	}

	public Set<Event> getAll() {
		return eventDao.getAll();
	}

	public Event getById(int id) {
		return eventDao.getById(id);
	}


	public void update(Event e) {
		eventDao.update(e);
	}

	public void delete(Event e) {
		eventDao.delete(e);
	}

}
