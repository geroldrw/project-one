package com.revature.services;

import java.util.Set;

import com.revature.beans.Award;
import com.revature.data.AwardDAO;
import com.revature.data.AwardOracle;

public class AwardServiceImpl implements AwardService {
	
	private static AwardDAO awardDao;
	
	public AwardServiceImpl()
	{
		awardDao = new AwardOracle();
	}
	
	public int add(Award a) {
		return awardDao.add(a);
	}

	public Set<Award> getAll() {
		return awardDao.getAll();
	}

	public Award getById(int id) {
		return awardDao.getById(id);
	}

	public Set<Award> getAwardsByReviewId(int revId) {
		return awardDao.getAwardsByReviewId(revId);
	}

	public Set<Award> getAwardsByUser(int userId) {
		return awardDao.getAwardsByUser(userId);
	}

	public void update(Award a) {
		awardDao.update(a);
	}

	public void delete(Award a) {
		awardDao.delete(a);
	}

}
