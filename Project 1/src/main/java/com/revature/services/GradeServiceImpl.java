package com.revature.services;

import java.util.Set;

import com.revature.beans.Grade;
import com.revature.data.GradeDao;
import com.revature.data.GradeOracle;

public class GradeServiceImpl implements GradeService {
	
	private static GradeDao gradeDao;
	
	public GradeServiceImpl()
	{
		gradeDao = new GradeOracle();
	}

	public int addGrade(Grade g) {
		return gradeDao.add(g);
	}

	public Set<Grade> getAllGrades() {
		return gradeDao.getAll();
	}

	public Grade getGradeById(int id) {
		return gradeDao.getById(id);
	}

	public Set<Grade> getPendingGrades() {
		return gradeDao.getPendingGrades();
	}

	public Set<Grade> getAcceptedGrades() {
		return gradeDao.getAcceptedGrades();
	}

	public void updateGrade(Grade g) {
		gradeDao.update(g);
	}

	public void deleteGrade(Grade g) {
		gradeDao.delete(g);
	}

}
