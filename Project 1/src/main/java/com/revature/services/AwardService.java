package com.revature.services;

import java.util.Set;

import com.revature.beans.Award;

public interface AwardService {
	public int add(Award a);
	public Set<Award> getAll();
	public Award getById(int id);
	public Set<Award> getAwardsByReviewId(int revId);
	public Set<Award> getAwardsByUser(int userId);
	public void update(Award a);
	public void delete(Award a);
}
