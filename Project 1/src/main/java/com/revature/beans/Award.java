package com.revature.beans;

public class Award {
	private int id;
	private int eventId;
	private int reviewId; //0 pending from Supervisor, 1 denied, 2 pending from Department Head,
			      //3 pending from Benefits Coop, 4 Accepted
	private int userId;
	private double amount;
	//

	public Award()
	{
		id = 0;
		eventId = 0;
		reviewId = 0;
		userId = 0;
		amount = 0.0;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public int getReviewId() {
		return reviewId;
	}

	public void setReviewId(int reviewId) {
		this.reviewId = reviewId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(amount);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + eventId;
		result = prime * result + id;
		result = prime * result + reviewId;
		result = prime * result + userId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Award other = (Award) obj;
		if (Double.doubleToLongBits(amount) != Double.doubleToLongBits(other.amount))
			return false;
		if (eventId != other.eventId)
			return false;
		if (id != other.id)
			return false;
		if (reviewId != other.reviewId)
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Award [id=" + id + ", eventId=" + eventId + ", reviewId=" + reviewId + ", userId=" + userId
				+ ", amount=" + amount + "]";
	}
	
	
}
