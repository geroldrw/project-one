package com.revature.beans;

public class Grade {
	private int id;
	private int reviewId; //0 pending, 1 pass, 2 fail
	private int formatId; //0 presentation, 1 letter grade, 2 pass/fail
	

	public Grade()
	{
		id = 0;
		reviewId = 0;
		formatId = 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getReviewId() {
		return reviewId;
	}

	public void setReviewId(int reviewId) {
		this.reviewId = reviewId;
	}

	public int getFormatId() {
		return formatId;
	}

	public void setFormatId(int formatId) {
		this.formatId = formatId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + formatId;
		result = prime * result + id;
		result = prime * result + reviewId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grade other = (Grade) obj;
		if (formatId != other.formatId)
			return false;
		if (id != other.id)
			return false;
		if (reviewId != other.reviewId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Grade [id=" + id + ", reviewId=" + reviewId + ", formatId=" + formatId + "]";
	}
	
	
}
