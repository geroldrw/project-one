package com.revature.data;

import com.revature.beans.User;

public interface UserDAO extends GenaricDAO<User> {
	public User getUserByUsernameAndPassword(String username, String password);
}
