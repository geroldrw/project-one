package com.revature.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.revature.beans.Info;
import com.revature.utils.ConnectionUtil;
import com.revature.utils.LogUtil;

public class InfoOracle implements InfoDAO {

	ConnectionUtil cu = ConnectionUtil.getConnectionUtil();
	Logger log = Logger.getLogger(InfoOracle.class);

	//create
	@Override
	public int add(Info t) {
		int key = 0; 
		log.trace("Adding info " + t);
		Connection conn = cu.getConnection();
		try {
			conn.setAutoCommit(false);
			String sql ="insert into people(street_address, city, state, zip, phone)"
					+ "values (?, ?, ?, ?, ?)";
			String[] keys = {"1d"};
			PreparedStatement pstmt = conn.prepareStatement(sql, keys);
			pstmt.setString(1, t.getAddress());
			pstmt.setString(2, t.getCity());
			pstmt.setString(3, t.getState());
			pstmt.setString(4, t.getZip());
			pstmt.setString(5, t.getPhone());
			
			
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				log.trace("new info successfully added.");
				key = rs.getInt(1);
			} else {
				log.trace("new info not added successfully.");
				conn.rollback();
			}
			
		}catch (SQLException e) {
			LogUtil.logException(e, InfoOracle.class);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				LogUtil.logException(e, InfoOracle.class);
			}
		}
		return key;
	}
	
	//select
	@Override
	public Set<Info> getAll() {
		Set<Info> info = new HashSet<Info>();
		log.trace("Getting all info from the database");
		try (Connection conn = cu.getConnection()) {
			conn.setAutoCommit(false);
			String sql = "select * from Info";
			
			PreparedStatement pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			
			
			while (rs.next())
			{
				Info p = new Info();
				p.setId(rs.getInt("id"));
				p.setAddress(rs.getString("street_address"));
				p.setCity(rs.getString("city"));
				p.setState(rs.getString("state"));
				p.setZip(rs.getString("zip_code"));
				p.setPhone(rs.getString("phone"));
				info.add(p);
			}	
		}catch (Exception e) { 
			LogUtil.logException(e, InfoOracle.class);
		}
		return null;
	}

	@Override
	public Info getById(int id) {
		Info p = null;
		log.trace("Getting info with id " + id);
		try (Connection conn = cu.getConnection()) {
			conn.setAutoCommit(false);
			String sql = "select * from info where id = ?";
			
			PreparedStatement pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, id);
			
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next())
			{
				p = new Info();
				log.trace("The info with id " + id + " found.");
				p.setId(rs.getInt("id"));
				p.setAddress(rs.getString("street_address"));
				p.setCity(rs.getString("city"));
				p.setState(rs.getString("state"));
				p.setZip(rs.getString("zip_code"));
				p.setPhone(rs.getString("phone"));
			}	
		}catch (Exception e) {
			LogUtil.logException(e, InfoOracle.class);
		}
		return p;
	}
	
	//update
	@Override
	public void update(Info t) {
		log.trace("Updating info with id " + t.getId());
		try (Connection conn = cu.getConnection())// why are we calling this method from connectionj
				{
					conn.setAutoCommit(false);
					String sql = "update user set street_address = ? ,"
							+ "city = ?, state = ?, zip_code = ? "
							+"phone = ?, where id = ?";
					PreparedStatement pstmt = conn.prepareStatement(sql);
					pstmt.setString(1, t.getAddress());
					pstmt.setString(2, t.getCity());
					pstmt.setString(3, t.getState());
					pstmt.setString(4, t.getZip());
					pstmt.setString(5, t.getPhone());
					
					int rowChanged = pstmt.executeUpdate();
					
					if (rowChanged > 0) {
						log.trace("info successfully updated.");
						conn.commit();
					} else {
						log.trace("No info found with that id.");
						conn.rollback();
					}
				} catch (Exception e) {
					LogUtil.logException(e, InfoOracle.class);
				}
	}

	//delete
	@Override
	public void delete(Info t) {
		log.trace("Removing info " + t);
				try (Connection conn = cu.getConnection()) {
					conn.setAutoCommit(false);
					
					String sql = "delete from info where id = ?";
					PreparedStatement pstmt = conn.prepareStatement(sql);
					pstmt.setInt(1, t.getId());
					
					int rowChanged = pstmt.executeUpdate();
					if (rowChanged > 0) {
						log.trace("Removed info successfully.");
						conn.commit();
					} else {
						log.trace("No info found with that id.");
						conn.rollback();
					}
			} catch (Exception e) {
				LogUtil.logException(e, InfoOracle.class);
			}
		
	}

}
