package com.revature.data;

import java.util.Set;

public interface GenaricDAO <T>{
		//create
		public int add(T t);
		//read
		public Set<T> getAll();
		public T getById(int id);
		//update
		public void update(T t);
		//delete
		public void delete(T t);
}
