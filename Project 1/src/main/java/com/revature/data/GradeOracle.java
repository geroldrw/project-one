package com.revature.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.revature.beans.Grade;
import com.revature.utils.ConnectionUtil;
import com.revature.utils.LogUtil;

public class GradeOracle implements GradeDao {

	ConnectionUtil cu = ConnectionUtil.getConnectionUtil();
	Logger log = Logger.getLogger(GradeOracle.class);
	
	public int add(Grade t) {
		int key = 0;
		log.trace("Adding grade "+ t);
		Connection conn = cu.getConnection();
		try
		{
			conn.setAutoCommit(false);
			String sql = "insert into grade_result(id, format_id, review_id)\r\n" + 
					"    values(?,?,?)";
			String[] keys = {"id"};
			PreparedStatement pstmt = conn.prepareStatement(sql, keys);
			pstmt.setInt(1, t.getId());
			pstmt.setInt(2, t.getFormatId());
			pstmt.setInt(3, t.getReviewId());
			
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if(rs.next())
			{
				log.trace("grade was successfully added!");
				key = rs.getInt(1);
				conn.commit();
			}else
			{
				log.trace("grade was not added successfully");
				conn.rollback();
			}
		} catch (SQLException e)
		{
			LogUtil.logException(e, GradeOracle.class);
		} finally
		{
			try
			{
				conn.close();
			}catch (SQLException e)
			{
				LogUtil.logException(e, GradeOracle.class);
			}
		}
		return key;
	}

	public Set<Grade> getAll() {
		Set<Grade> grades = new HashSet<Grade>();
		log.trace("Getting all grades from the database.");
		try(Connection conn = cu.getConnection())
		{
			String sql = "select id, format_id, review_id from grade_result";
			Statement stmt =conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next())
			{
				Grade g = new Grade();
				g.setId(rs.getInt("id"));
				g.setFormatId(rs.getInt("format_id"));
				g.setReviewId(rs.getInt("review_id"));
				grades.add(g);
			}
			
		}catch (Exception e)
		{
			LogUtil.logException(e, GradeOracle.class);
		}
		return grades;
	}

	public Grade getById(int id) {
		Grade g = null;
		log.trace("Getting a grade by id.");
		try (Connection conn = cu.getConnection())
		{
			String sql = "select id, format_id, review_id from grade_result where id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1,id);
			ResultSet rs = pstmt.executeQuery();
			if(rs.next())
			{
				log.trace("grade with id " + id +" found.");
				g = new Grade();
				g.setId(id);
				g.setFormatId(rs.getInt("format_id"));
				g.setReviewId(rs.getInt("review_id"));
			}
			else
			{
				log.trace("No grade found with that id.");
			}
		} catch (Exception e)
		{
			LogUtil.logException(e, GradeOracle.class);
		}
		return g;
	}

	public void update(Grade t) {
		log.trace("Updating grade with id " + t.getId());
		try (Connection conn = cu.getConnection())
		{
			conn.setAutoCommit(false);
			String sql = "update grade_result set format_id = ?, review_id = ? where id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, t.getFormatId());
			pstmt.setInt(2, t.getReviewId());
			pstmt.setInt(3, t.getId());
		
			int rowsAffected = pstmt.executeUpdate();
			if (rowsAffected > 0) 
			{
				log.trace("Grade successfully updated.");
				conn.commit();
			} else {
				log.trace("No grade found with that id.");
				conn.rollback();
			}
		} catch (Exception e) {
			LogUtil.logException(e, GradeOracle.class);
		}
	}

	public void delete(Grade t) {
		log.trace("Deleting grade " + t);
		try (Connection conn = cu.getConnection()) {
			conn.setAutoCommit(false);
			
			String sql = "delete from grade_result where id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, t.getId());
			
			int rowsAffected = pstmt.executeUpdate();
			if (rowsAffected > 0) {
				log.trace("Removed grade successfully.");
				conn.commit();
			} else {
				log.trace("No grade found with that id.");
				conn.rollback();
			}
		} catch (Exception e) {
			LogUtil.logException(e, GradeOracle.class);
		}
	}

	public Set<Grade> getPendingGrades() {
		Set<Grade> grades = new HashSet<Grade>();
		log.trace("Getting all pending grades from the database.");
		try(Connection conn = cu.getConnection())
		{
			String sql = "select id, format_id, review_id from grade_result where review_id = 0";
			Statement stmt =conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next())
			{
				Grade g = new Grade();
				g.setId(rs.getInt("id"));
				g.setFormatId(rs.getInt("format_id"));
				g.setReviewId(rs.getInt("review_id"));
				grades.add(g);
			}
			
		}catch (Exception e)
		{
			LogUtil.logException(e, GradeOracle.class);
		}
		return grades;
	}

	public Set<Grade> getAcceptedGrades() {
		Set<Grade> grades = new HashSet<Grade>();
		log.trace("Getting all approved grades from the database.");
		try(Connection conn = cu.getConnection())
		{
			String sql = "select id, format_id, review_id from grade_result where review_id = 1";
			Statement stmt =conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next())
			{
				Grade g = new Grade();
				g.setId(rs.getInt("id"));
				g.setFormatId(rs.getInt("format_id"));
				g.setReviewId(rs.getInt("review_id"));
				grades.add(g);
			}
			
		}catch (Exception e)
		{
			LogUtil.logException(e, GradeOracle.class);
		}
		return grades;
	}

}
