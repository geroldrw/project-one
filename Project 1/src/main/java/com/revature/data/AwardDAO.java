package com.revature.data;

import java.util.Set;

import com.revature.beans.Award;

public interface AwardDAO extends GenaricDAO<Award> {
	//read
	public Set<Award> getAwardsByReviewId(int revId);
	public Set<Award> getAwardsByUser(int userId);
}
