package com.revature.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.revature.beans.Event;
import com.revature.utils.ConnectionUtil;
import com.revature.utils.LogUtil;

public class EventOracle implements EventDAO {

	ConnectionUtil cu = ConnectionUtil.getConnectionUtil();
	Logger log = Logger.getLogger(EventOracle.class);
	
	public int add(Event t) {
		int key = 0;
		log.trace("Adding event "+ t);
		Connection conn = cu.getConnection();
		try
		{
			conn.setAutoCommit(false);
			String sql = "insert into event_type(id, cost, type, grade_id)\r\n"
					+ "values(?,?,?,?)";
			String[] keys = {"id"};
			PreparedStatement pstmt = conn.prepareStatement(sql, keys);
			pstmt.setInt(1, t.getId());
			pstmt.setDouble(2, t.getCost());
			pstmt.setString(3, t.getType());
			pstmt.setInt(4,  t.getGradeId());
			
			pstmt.executeUpdate();
			ResultSet rs = pstmt.getGeneratedKeys();
			if(rs.next())
			{
				log.trace("Event was successfully added!");
				key = rs.getInt(1);
				conn.commit();
			}else
			{
				log.trace("Event was not added successfully");
				conn.rollback();
			}
		} catch (SQLException e)
		{
			LogUtil.logException(e, EventOracle.class);
		} finally
		{
			try
			{
				conn.close();
			}catch (SQLException e)
			{
				LogUtil.logException(e, EventOracle.class);
			}
		}
		return key;
	}

	public Set<Event> getAll() {
		Set<Event> events = new HashSet<Event>();
		log.trace("Getting all events from the database.");
		try(Connection conn = cu.getConnection())
		{
			String sql = "select id, cost, type, grade_id from event_type";
			Statement stmt =conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next())
			{
				Event e = new Event();
				e.setId(rs.getInt("id"));
				e.setCost(rs.getDouble("cost"));
				e.setType(rs.getString("type"));
				e.setGradeId(rs.getInt("grade_id"));
				
				events.add(e);
			}
			
		}catch (Exception e)
		{
			LogUtil.logException(e, EventOracle.class);
		}
		return events;
	}

	public Event getById(int id) {
		Event ev = null;
		log.trace("Getting an event by id.");
		try (Connection conn = cu.getConnection())
		{
			String sql = "select id, cost, type, grade_id from event_type where id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1,id);
			ResultSet rs = pstmt.executeQuery();
			if(rs.next())
			{
				log.trace("Event with id " + id +" found.");
				ev = new Event();
				ev.setId(id);
				ev.setCost(rs.getDouble("cost"));
				ev.setType(rs.getString("type"));
				ev.setGradeId(rs.getInt("grade_id"));
			}
			else
			{
				log.trace("No event found with that id.");
			}
		} catch (Exception e)
		{
			LogUtil.logException(e, EventOracle.class);
		}
		return ev;
	}

	public void update(Event t) {
		log.trace("Updating event with id " + t.getId());
		try (Connection conn = cu.getConnection())
		{
			conn.setAutoCommit(false);
			String sql = "update people set cost = ?, type = ?, grade_id = ? where id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setDouble(1, t.getCost());
			pstmt.setString(2, t.getType());
			pstmt.setInt(3, t.getGradeId());
			pstmt.setInt(4, t.getId());
		
			int rowsAffected = pstmt.executeUpdate();
			if (rowsAffected > 0) 
			{
				log.trace("Event successfully updated.");
				conn.commit();
			} else {
				log.trace("No event found with that id.");
				conn.rollback();
			}
		} catch (Exception e) {
			LogUtil.logException(e, EventOracle.class);
		}
	}

	public void delete(Event t) {
		log.trace("Deleting event " + t);
		try (Connection conn = cu.getConnection()) {
			conn.setAutoCommit(false);
			
			String sql = "delete from event_type where id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, t.getId());
			
			int rowsAffected = pstmt.executeUpdate();
			if (rowsAffected > 0) {
				log.trace("Removed event successfully.");
				conn.commit();
			} else {
				log.trace("No event found with that id.");
				conn.rollback();
			}
		} catch (Exception e) {
			LogUtil.logException(e, EventOracle.class);
		}
	}
}
	