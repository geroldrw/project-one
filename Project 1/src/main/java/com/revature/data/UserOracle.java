package com.revature.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.revature.beans.User;
import com.revature.utils.ConnectionUtil;
import com.revature.utils.LogUtil;

public class UserOracle implements UserDAO {

	ConnectionUtil cu = ConnectionUtil.getConnectionUtil();
	Logger log = Logger.getLogger(UserOracle.class);

	//create
	public int add(User t) {
		Integer key = 0; 
		log.trace("Adding a person " + t);
		Connection conn = cu.getConnection();
		try {
			conn.setAutoCommit(false);
			String sql ="insert into people( first_name, last_name, username, user_password, email, amount_available, info_id, supervisor_id, job_title_id )"
					+ "values (?, ?, ?, ?, ?, ?, ?, ?)";
			String[] keys = {"1d"};
			PreparedStatement pstmt = conn.prepareStatement(sql, keys);
			pstmt.setString(1, t.getFirstName());
			pstmt.setString(2, t.getLastName());
			pstmt.setString(3, t.getUsername());
			pstmt.setString(4, t.getPassword());
			pstmt.setDouble(5, t.getAvailableReimbursment());
			pstmt.setInt(6, t.getInfoId());
			pstmt.setInt(7, t.getReportsTo());
			pstmt.setInt(8, t.getJobId());
			
			pstmt.executeUpdate(); 
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				log.trace("new user successfully added.");
				key = rs.getInt(1);
				conn.commit();
			} else {
				log.trace("new user not added successfully.");
				conn.rollback();
			}
			
		}catch (SQLException e) {
			LogUtil.logException(e, UserOracle.class);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				LogUtil.logException(e, UserOracle.class);
			}
		}
		return key;
	}
	//select
	public User getById(int id) {
		User p = null;
		log.trace("Getting a person with id " + id);
		try (Connection conn = cu.getConnection()) {
			conn.setAutoCommit(false);
			String sql = "select * from User where id = ?";
			
			PreparedStatement pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, id);
			
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next())
			{
				p = new User();
				log.trace("The Person with id " + id + " found.");
				p.setId(rs.getInt("id"));
				p.setFirstName(rs.getString("firstName"));
				p.setLastName(rs.getString("lastName"));
				p.setUsername(rs.getString("Username"));
				p.setPassword(rs.getString("user_Password"));
				p.setEmail(rs.getString("email"));
				p.setAvailableReimbursment(rs.getDouble("amount_available"));
				p.setInfoId(rs.getInt("info_id"));
				p.setReportsTo(rs.getInt("supervisor_id"));
				p.setJobId(rs.getInt("supervisor_id"));
			}	
		}catch (Exception e) {
			LogUtil.logException(e, UserOracle.class);
		}
		return p;
	}

	public Set<User> getAll() {
		Set<User> users = new HashSet<User>();
		log.trace("Getting all users from the database");
		try (Connection conn = cu.getConnection()) {
			conn.setAutoCommit(false);
			String sql = "select * from user";
			
			PreparedStatement pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			
			
			while (rs.next()) {
				User p = new User();
				p.setId(rs.getInt("id"));
				p.setFirstName(rs.getString("first_Name"));
				p.setLastName(rs.getString("last_Name"));
				p.setUsername(rs.getString("Username"));
				p.setPassword(rs.getString("user_Password"));
				p.setEmail(rs.getString("email"));
				p.setAvailableReimbursment(rs.getDouble("amount_available"));
				p.setInfoId(rs.getInt("info_id"));
				p.setReportsTo(rs.getInt("supervisor_id"));
				p.setJobId(rs.getInt("supervisor_id"));
				users.add(p);
			}	
		}catch (Exception e) { 
			LogUtil.logException(e, UserOracle.class);
		}
		return null;
	}
	public User getUserByUsernameAndPassword(String username, String password) {
		User p = null;   
		log.trace("getting the user by username and password " );
		try (Connection conn = cu.getConnection()) {
			conn.setAutoCommit(false);
			String sql = "select * from user where username = ? and user_password = ?"; //need to figure out how add a passwrod query as well
			
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, username);
			pstmt.setString(2, password);
			
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next())//what is the .next() method
			{
				p = new User();
				p.setId(rs.getInt("id"));
				p.setFirstName(rs.getString("first_Name"));
				p.setLastName(rs.getString("last_Name"));
				p.setUsername(rs.getString("Username"));
				p.setPassword(rs.getString("user_Password"));
				p.setEmail(rs.getString("email"));
				p.setAvailableReimbursment(rs.getDouble("amount_available"));
				p.setInfoId(rs.getInt("info_id"));
				p.setReportsTo(rs.getInt("supervisor_id"));
				p.setJobId(rs.getInt("supervisor_id"));
			}	
		}catch (Exception e) {
			LogUtil.logException(e, UserOracle.class);
		}
		return p;
	}

	//update
	public void update(User t) {
		log.trace("Updating user with id " + t.getId());
		try (Connection conn = cu.getConnection())// why are we calling this method from connectionj
		{
			conn.setAutoCommit(false);
			String sql = "update user set first_name = ? "
					+ "last_name = ?, username = ?, user_password = ? "
					+"email = ?, Amount_available = ?, job_title_id = ? "
					+"supervisor_id = ?, user_id =? where id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, t.getFirstName());
			pstmt.setString(2, t.getLastName());
			pstmt.setString(3, t.getUsername());
			pstmt.setString(4, t.getPassword());
			pstmt.setDouble(5, t.getAvailableReimbursment());
			pstmt.setInt(6, t.getInfoId());
			pstmt.setInt(7, t.getReportsTo());
			pstmt.setInt(8, t.getJobId());
			
			int rowChanged = pstmt.executeUpdate();
			
			if (rowChanged > 0) {
				log.trace("user successfully updated.");
				conn.commit();
			} else {
				log.trace("No user found with that id.");
				conn.rollback();
			}
		} catch (Exception e) {
			LogUtil.logException(e, UserOracle.class);
		}
	}

	//delete
	public void delete(User t) {
		log.trace("Removing user " + t);
		try (Connection conn = cu.getConnection()) {
			conn.setAutoCommit(false);
			
			String sql = "delete from user where id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, t.getId());
			
			int rowChanged = pstmt.executeUpdate();
			if (rowChanged > 0) {
				log.trace("Removed user successfully.");
				conn.commit();
			} else {
				log.trace("Nobody found with that id.");
				conn.rollback();
			}
	} catch (Exception e) {
		LogUtil.logException(e, UserOracle.class);
	}
		
	
}
}
