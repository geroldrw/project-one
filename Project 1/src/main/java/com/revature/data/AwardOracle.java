package com.revature.data;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import com.revature.beans.Award;
import com.revature.utils.ConnectionUtil;
import com.revature.utils.LogUtil;

public class AwardOracle implements AwardDAO {

	ConnectionUtil cu = ConnectionUtil.getConnectionUtil();
	Logger log = Logger.getLogger(UserOracle.class);
	
	public int add(Award t) {
		int key = 0; 
		log.trace("Adding an award " + t);
		Connection conn = cu.getConnection();
		try {
			conn.setAutoCommit(false);
			String sql ="insert into event_type(id, cost, type, grade_id)\r\n" + 
										"values(?,?,?,?)";
			String[] keys = {"id"};
			PreparedStatement pstmt = conn.prepareStatement(sql, keys);
			pstmt.setInt(1, t.getEventId());
			pstmt.setInt(2, t.getReviewId());
			pstmt.setInt(3, t.getUserId());
			pstmt.setDouble(4, t.getAmount());
			
			pstmt.executeUpdate(); 
			ResultSet rs = pstmt.getGeneratedKeys();
			if (rs.next()) {
				log.trace("new award successfully added.");
				key = rs.getInt(1);
				conn.commit();
			} else {
				log.trace("new award not added successfully.");
				conn.rollback();
			}
			
		}catch (SQLException e) {
			LogUtil.logException(e, AwardOracle.class);
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				LogUtil.logException(e, AwardOracle.class);
			}
		}
		return key;
	}

	public Set<Award> getAll() {
		Set<Award> awards = new HashSet<Award>();
		log.trace("Getting all awards from the database.");
		try(Connection conn = cu.getConnection())
		{
			String sql = "select id, event_id, is_pending, user_id, amount from awarded";
			Statement stmt =conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			
			while (rs.next())
			{
				Award a = new Award();
				a.setId(rs.getInt("id"));
				a.setEventId(rs.getInt("event_id"));
				a.setReviewId(rs.getInt("is_pending"));
				a.setUserId(rs.getInt("user_id"));
				a.setAmount(rs.getDouble("amount"));
				awards.add(a);
			}
			
		}catch (Exception e)
		{
			LogUtil.logException(e, AwardOracle.class);
		}
		return awards;
	}
	

	public Award getById(int id) {
		Award a = null;
		try (Connection conn = cu.getConnection()) {
			conn.setAutoCommit(false);
			String sql = "select id, event_id, is_pending, user_id, amount from awarded where id = ?";
			
			PreparedStatement pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, id);
			
			
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next())
			{
				a = new Award();
				log.trace("The award with id " + id + " found.");
				a.setId(rs.getInt("id"));
				a.setEventId(rs.getInt("event_id"));
				a.setReviewId(rs.getInt("is_pending"));
				a.setUserId(rs.getInt("user_id"));
				a.setAmount(rs.getDouble("amount"));
				
			}	
		}catch (Exception e) {
			LogUtil.logException(e, AwardOracle.class);
		}
		return a;
	}

	public void update(Award t) {
		log.trace("Updating award with id " + t.getId());
		try (Connection conn = cu.getConnection())
		{
			conn.setAutoCommit(false);
			String sql = "update awarded set event_id = ?, is_pending = ?, user_id = ?, amount = ? where id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, t.getEventId());
			pstmt.setInt(2, t.getReviewId());
			pstmt.setInt(3, t.getUserId());
			pstmt.setDouble(4, t.getAmount());
			pstmt.setInt(5, t.getId());;
		
			int rowsAffected = pstmt.executeUpdate();
			if (rowsAffected > 0) 
			{
				log.trace("Awarded successfully updated.");
				conn.commit();
			} else {
				log.trace("No award found with that id.");
				conn.rollback();
			}
		} catch (Exception e) {
			LogUtil.logException(e, AwardOracle.class);
		}
	}

	public void delete(Award t) {
		log.trace("Deleting award " + t);
		try (Connection conn = cu.getConnection()) {
			conn.setAutoCommit(false);
			
			String sql = "delete from awarded where id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, t.getId());
			
			int rowsAffected = pstmt.executeUpdate();
			if (rowsAffected > 0) {
				log.trace("Removed award successfully.");
				conn.commit();
			} else {
				log.trace("No award found with that id.");
				conn.rollback();
			}
		} catch (Exception e) {
			LogUtil.logException(e, AwardOracle.class);
		}
	}

	public Set<Award> getAwardsByReviewId(int revId) {
		Set<Award> awards = new HashSet<Award>();
		log.trace("Getting all awards with the review id " + revId + " from the database.");
		try(Connection conn = cu.getConnection())
		{
			String sql = "select id, event_id, is_pending, user_id, amount from awarded where is_pending = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, revId);
			
			
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next())
			{
				Award a = new Award();
				a.setId(rs.getInt("id"));
				a.setEventId(rs.getInt("event_id"));
				a.setReviewId(rs.getInt("is_pending"));
				a.setUserId(rs.getInt("user_id"));
				a.setAmount(rs.getDouble("amount"));
				awards.add(a);
			}
			
		}catch (Exception e)
		{
			LogUtil.logException(e, AwardOracle.class);
		}
		return awards;
	}

	public Set<Award> getAwardsByUser(int userId) {
		Set<Award> awards = new HashSet<Award>();
		log.trace("Getting all awards with the user id " + userId + " from the database.");
		try(Connection conn = cu.getConnection())
		{
			String sql = "select id, event_id, is_pending, user_id, amount from awarded where user_id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			
			pstmt.setInt(1, userId);
			
			
			ResultSet rs = pstmt.executeQuery();
			
			while (rs.next())
			{
				Award a = new Award();
				a.setId(rs.getInt("id"));
				a.setEventId(rs.getInt("event_id"));
				a.setReviewId(rs.getInt("is_pending"));
				a.setUserId(rs.getInt("user_id"));
				a.setAmount(rs.getDouble("amount"));
				awards.add(a);
			}
			
		}catch (Exception e)
		{
			LogUtil.logException(e, AwardOracle.class);
		}
		return awards;
	}

}
