package com.revature.data;

import java.util.Set;

import com.revature.beans.Grade;

public interface GradeDao extends GenaricDAO<Grade> {
	//read
	public Set<Grade> getPendingGrades();
	public Set<Grade> getAcceptedGrades();
}
